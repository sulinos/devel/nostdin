APPLETS= blackhole nostdin stdswp stdmerge nostd
SHELL=/bin/bash -ex
build:
	for applet in $(APPLETS) ; do \
	    $(CC) $(CFLAGS) -Wno-format-security -o $$applet $$applet.c run.c -I . ; \
	done
install:
	mkdir -p $(DESTDIR)/bin
	for applet in $(APPLETS) ; do \
	    install $$applet $(DESTDIR)/bin/$$applet ; \
	done
clean:
	for applet in $(APPLETS) ; do \
	    rm -f nostdin $$applet ;\
	done

