#include <run.h>
#include <stdio.h>
#include <unistd.h>
int main(int argc,char *argv[]){
    fclose(stdin);
     if (argc<2){
        puts("Usage: nostd [command]");
        return 1;
    }
    int devnull = fileno(fopen("/dev/null","r"));
    dup2(devnull, STDOUT_FILENO);
    dup2(devnull, STDERR_FILENO);
    run(argc,argv);
}

