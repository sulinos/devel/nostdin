# Tools

* blackhole : eat stdin
* nostdin : run command without stdin
* stdswp : swap stderr and stdout
* stdmerge : merge stderr and stdout
* nostd : run commnand without stdin, stdout and stderr

## stdin stdout stderr

* stdin = input
* stdout = output
* stderr = error output
