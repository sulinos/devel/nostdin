#include <stdio.h>
#include <unistd.h>
#include <run.h>
int main(int argc,char *argv[]){
    /* Copy fileno*/
    int stdout_copy = dup(STDOUT_FILENO);
    /*merge fileno*/
    dup2(stdout_copy, STDOUT_FILENO);
    dup2(stdout_copy, STDERR_FILENO);
    /*execute*/
    run(argc,argv);
}
