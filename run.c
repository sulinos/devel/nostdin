#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>

#include <run.h>
int run(int argc,char *argv[]){
    if (argc<2){
        printf("Usage: ");
        printf(argv[0]);
        printf(" [command]\n");
        return 1;
    }
    char *cmd[argc];
    for(int i=0;i<argc-1;i++){
        cmd[i] = argv[i+1];
    }
    cmd[argc-1] = NULL;
    setenv("DEBIAN_FRONTEND","noninteractive",1);
    execvp(which(argv[1]),cmd);
}

char* which(char* cmd){
    char* fullPath = getenv("PATH");
    struct stat buffer;
    int exists;
    char* fileOrDirectory = cmd;
    char *fullfilename = malloc(1024*sizeof(char));

    char *token = strtok(fullPath, ":");

    /* walk through other tokens */
    while( token != NULL )
    {
        sprintf(fullfilename, "%s/%s", token, fileOrDirectory);
        exists = stat( fullfilename, &buffer );
        if ( exists == 0 && ( S_IFREG & buffer.st_mode ) ) {
            char ret[strlen(fullfilename)];
            strcpy(ret,fullfilename);
            return (char*)fullfilename;
        }

        token = strtok(NULL, ":"); /* next token */
    }
    return "";
}
